
(function($) {

    sizeScreen();

    $(window).on('resize', function() {
        sizeScreen();
    });
    function sizeScreen() {
        if(isMobile) {
            return;
        }
        var height = $(window).height();
        var width = $(document).width();
        $('.size').html(width + ' px / '+width/16 + ' em');
        $('.max-height').css({
            'height':height+'px'
        });
    }

})(jQuery);
