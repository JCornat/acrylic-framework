
(function($) {
    $(document).on('click', '.alert a.alert-close', function(e) {
        e.preventDefault();
        $(this).closest('.alert').fadeOut(500).queue(function() {
            $(this).closest('.alert').remove();
        });
    });

    $('.spawn-alert-error').on('click', function(e) {
        e.preventDefault();
        $('body').append('<div class="alert alert-error float"><a href="#" class="alert-close" data-dismiss="alert">&times;</a>Ceci est une alerte d\'erreur.</div>');
    });

    $('.spawn-alert-success').on('click', function(e) {
        e.preventDefault();
        $('body').append('<div class="alert alert-success float"><a href="#" class="alert-close" data-dismiss="alert">&times;</a>Ceci est une alerte de succès.</div>');
    });

    $('.spawn-alert-info').on('click', function(e) {
        e.preventDefault();
        $('body').append('<div class="alert alert-info float"><a href="#" class="alert-close" data-dismiss="alert">&times;</a>Ceci est une alerte d\'information.</div>');
    });

})(jQuery);
