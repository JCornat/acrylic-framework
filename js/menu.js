
(function($) {

    $('.dropdown').on('click', function(e) {
        if(!$(this).hasClass('open')) {
            $(this).addClass('open', 'true');
            $(this).find('ul').first().stop(true, true).slideDown();
            $(this).find('.fa').first().removeClass('fa-caret-down').addClass('fa-caret-up');
        } else {
            $(this).removeClass('open');
            $(this).find('ul').first().stop(true, true).slideUp();
            $(this).find('.fa').first().removeClass('fa-caret-up').addClass('fa-caret-down');
        }
    });

    $(window).resize(function() {
        if(!$('.menu > button').is(':visible')){
            $('.menu > ul').css('display','');
        }
    });

    $('.menu button.nav').on('click', function(e) {
        if(!$(this).hasClass('open')) {
            $(this).addClass('open', 'true');
            $('.menu ul').stop(true, true).slideDown();
        } else {
            $(this).removeClass('open');
            $('.menu ul').stop(true, true).slideUp();
        }
    });
})(jQuery);
